# **Designing a REST API**

The task is [HERE](https://gitlab.com/karinafed27/smallbusinessplatform/-/wikis/REST-API)


- Describe what entities it must use.
- Both functional and non-functional requirements should be provided.
- The model should be complete and unambiguous.
- All operation descriptions should be complete.
- All the status codes should be meaningful and mapped to their description.
- The Richardson Maturity Model should be applied.
- Errors and the authentication method should be described.
- All relevant methods should be paginated.
- Caching should be described, and all required methods should be cached.

![img.png](img.png)